import numpy  as  np
import pandas as pd

from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix

groups_dict = {
    0: 'country' ,
    1: 'hip-pop' ,
    2: 'metal' ,
    3: 'raggae' ,
    4: 'rock'
}


def train_test_split(dataset , prop):
    d = np.copy ( np.array ( dataset ) )
    sample_size = d.shape[ 0 ]

    ts_idx = np.random.randint ( 0 , sample_size , round ( sample_size * prop ) )
    tr_idx = np.delete ( np.arange ( 0 , sample_size ) , ts_idx )

    return d[ ts_idx ] , d[ tr_idx ]


def kfold(dataset , y , clf , k_list , fcc_list , cv=10):
    ts , tr = train_test_split ( dataset , 0.3 )

    y_tr = [ y[ int ( i ) ] for i in tr[ : , 0 ] ]
    y_ts = [ y[ int ( i ) ] for i in ts[ : , 0 ] ]

    test_score , train_score = [ ] , [ ]

    for fc in fcc_list:

        for k in k_list:
            clf.n_neighbors = k
            train_score.append ( np.mean ( cross_val_score ( clf , tr[ : , 1:fc ] , y_tr , cv=cv ) ) )

            clf.fit ( ts[ : , 1:fc ] , y_ts )
            test_score.append ( clf.score ( ts[ : , 1:fc ] , y_ts ) )

    all_score = train_score + test_score

    train_columns = [ 'train_acc_{}'.format ( k ) for k in k_list ]
    test_columns = [ 'test_acc_{}'.format ( k ) for k in k_list ]

    df = pd.DataFrame ( np.reshape ( all_score , (-1 , len ( k_list ) * 2) ) ,
                        columns=train_columns + test_columns )
    r = max ( df[ train_columns ].idxmax () )
    c = max ( df[ train_columns ].idxmax ( axis=1 ) )

    df.insert ( 0 , 'fcc' , fcc_list )

    idx = test_columns.index ( c )

    return df , (fcc_list[ r ] , k_list[ idx ])


def train(clf , x , y):
    y_ts = [ y[ i.astype ( 'int' ) ] for i in x[ : , 0 ] ]

    clf.fit ( x[ : , 1: ] , y_ts )
    score = clf.score ( x[ : , 1: ] , y_ts )

    return clf , score


def get_predictions(clf , data , verbose=True):
    y_hat = clf.predict ( data[ : , 1: ] )

    y_hat = np.column_stack ( [ data[ : , 0 ] , y_hat ] )
    df = pd.DataFrame ( y_hat , columns=[ 'song_id' , 'votes' ] )

    g = df.groupby ( 'song_id' , as_index=True ).votes
    df_g = pd.DataFrame ( g.aggregate ( lambda x: list ( x ) ) , columns=[ 'votes' ] )
    df_g.index = df_g.index.astype ( 'int' )

    for k in sorted ( df[ 'votes' ].unique () ):
        df_g[ int ( k ) ] = np.nan

    for i in df_g.index:
        v = pd.Series ( df_g.loc[ i , 'votes' ] ).value_counts ()
        for j in v.index:
            df_g.loc[ i , j ] = v[ j ]

    df_g = df_g.drop ( 'votes' , 1 )

    df_g[ 'y_hat' ] = df_g[ [ 0 , 1 , 2 , 3 , 4 ] ].idxmax ( axis=1 )

    df_g.rename_axis ( mapper=groups_dict , axis=1 , inplace=True )

    return df_g if verbose else df_g[ 'y_hat' ]


def score(df , y):
    assert type ( df ) is pd.core.frame.DataFrame

    miss_cl = np.mean ( df[ 'y_hat' ] != y )

    df[ 'mean' ] = df[ list ( groups_dict.values () ) ].mean ( axis=1 )
    df[ 'std' ] = df[ list ( groups_dict.values () ) ].std ( axis=1 )

    df.insert ( 0 , 'y_true' , y )

    cm = confusion_matrix ( df[ 'y_hat' ] , y )

    return miss_cl , df , cm
