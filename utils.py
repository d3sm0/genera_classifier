import numpy as np

import os
import pickle

import librosa
from sklearn.preprocessing import scale , LabelEncoder
import pandas as pd

MAX_FRAMES = 500
TARGET_SR = 11025


def load_labels(data_dir):
    labels = pd.read_table ( data_dir , delim_whitespace=True )

    lab = LabelEncoder ()
    l = lab.fit_transform ( np.ravel ( labels ) )
    return l.tolist () , lab.classes_


def load_songs(data_dir):
    file_list = librosa.util.find_files ( data_dir )
    X = [ ]

    for f in file_list:
        x , fs = librosa.load ( f )
        X.append ( x )
    return X , fs


def get_fcc(x , fs , n_fcc):
    x_dwn = librosa.resample ( x , orig_sr=fs , target_sr=TARGET_SR )
    S = librosa.core.stft ( x_dwn , n_fft=2048 , win_length=2048 , hop_length=512 )
    f_coef = np.dot ( librosa.filters.dct ( n_fcc , S.shape[ 0 ] ) , librosa.power_to_db ( S ) )

    return scale ( f_coef , axis=0 )[ : , :MAX_FRAMES ].T


def build_dataset(data , fs , n_fcc=15):
    features = [ ]

    for k in range ( len ( data ) ):
        X = get_fcc ( data[ k ] , fs , n_fcc )

        features.append ( np.append (
            np.full ( (X.shape[ 0 ] , 1) , fill_value=k ) ,
            X ,
            axis=1 ) )
    dataset = np.vstack ( np.array ( features ) )
    return dataset


def get_features(S):
    assert S.shape[ 0 ] >= 1020

    nb = 8
    low_band = 100
    eps = np.finfo ( float ).eps
    # number of seconds of the analyized window
    ntm = S.shape[ 1 ]
    corrtime = 15
    fs = 1025

    fourier_coeff = np.array ( [ low_band * (fs / 2 / low_band) ** (np.arange ( nb ) / nb) / fs * 2048 ] ).round ()

    fco = np.ndarray.astype ( np.append ( 0 , fourier_coeff ) , 'int' )

    energy = np.zeros ( shape=(nb , ntm) )

    for tm in range ( ntm ):
        for i in range ( nb ):
            lower_bound = 1 + fco[ i ]
            upper_bound = min ( 1 + fco[ i + 1 ] , S.shape[ 0 ] )
            energy[ i , tm ] = np.sum ( np.absolute ( S[ lower_bound:upper_bound , tm ] ) ** 2 )

    energy[ energy < eps ] = eps
    energy = 10 * np.log10 ( energy )
    return scale ( energy , axis=0 )


def save_vars(file_list , var_list):
    for file , v in zip ( file_list , var_list ):
        data_dir = os.path.join ( os.getcwd () , 'model/' , file )
        try:
            with open ( data_dir , 'wb' ) as f:
                pickle.dump ( v , f )
        except Exception as e:
            return e
    print ( 'model saved' )


def load_vars(file_list):
    var_list = [ ]
    for file in file_list:
        data_dir = os.path.join ( os.getcwd () , 'model/' , file )
        with open ( data_dir , 'rb' ) as f:
            var_list.append ( pickle.load ( f ) )
    return var_list
