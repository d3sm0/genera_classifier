import librosa
import utils
import train
import argparse
import numpy as np

np.random.seed ( 1234 )

parser = argparse.ArgumentParser(description='Get genere of a song')

parser.add_argument('--song_dir', metavar='song_dir', help = 'Directory of the song')


def main(song):

    file_list = [ 'data.pickle' , 'model.pickle']

    try:
        train_data, model = utils.load_vars(file_list)
    except Exception as e:
        return e

    song , fs = librosa.load ( song )


    features =   utils.build_dataset([song], fs, model['fcc_star'])

    clf = model['clf']

    assert model['k_star'] == clf.n_neighbors

    y_hat = train.get_predictions(clf, features[:, :model[ 'fcc_star' ]], verbose = False)
    label = train_data[ 'groups' ][ y_hat[ 0 ] ]
    print('The song is {}'.format(label))



if __name__ == "__main__":
    args = parser.parse_args()
    main(args.song_dir)


