Genera classifier based on kNN

To download the data and play with it, get it here [link](https://drive.google.com/open?id=0B3B22Hd5PMxSaVpsYmRHU2ZJYWc)

You can find the implementation in the notebook. To get music genera for a single song:

```
python main.py --song_dir 'my_song.mp3'
```

Requirements:
- librosa
- numpy
- pandas